#include "head.h"

int main(int argc, char **argv){
    int *arrayNumeros; //arreglo que tendrá los argumentos ya casteados
    int longitudArray = argc - 1; //longitud del array igual a la cantidad de argumentos -1 porque se descuenta el argumento del nombre
    pthread_t *hilo; //vector de hilos

    contador = 0;
    cond_recibida = 0;
    printf("Inicio del programa\n");
    if(validarInput(argc, argv)){
        arrayNumeros = extraerNumeros(argc, argv); //retorna un vector con los elementos del array ya casteados

        hilo = (pthread_t *) malloc( sizeof(pthread_t) * longitudArray ); //la cantidad de elementos es igual a la cantidad de hilos
        cv = (pthread_cond_t *)malloc( sizeof(pthread_cond_t) *longitudArray );

        for(int i = 0; i < longitudArray; i++){
            pthread_cond_init(&cv[ i ], NULL);
        }
        
        ptrHiloData hiloAuxiliar = (ptrHiloData)malloc(sizeof(hiloData) * longitudArray);

        pthread_mutex_init(&lock, NULL);

        for (int i = 0; i < longitudArray; i++){
            hiloAuxiliar[ i ].numHilo = i;
            hiloAuxiliar[ i ].value = arrayNumeros[ i ];
            pthread_create(&hilo[ i ], NULL, funcionHilo, &hiloAuxiliar[ i ]); //se crea un hilo por cada argumento
        }
        while(contador < longitudArray);
        for (int i = 0; i < longitudArray; i++){
            while(pthread_cond_signal(&cv[ i ]) != 0 || cond_recibida != 1);
            pthread_mutex_lock(&lock);
            cond_recibida = 0;
            pthread_mutex_unlock(&lock);
            pthread_join(hilo[ i ], NULL); //se espera a cada hilo
        }
        pthread_mutex_destroy(&lock);
        for(int i = 0; i < longitudArray; i++){
            pthread_cond_destroy(&cv[ i ]);
        }
        free(hilo);
        free(cv);
        free(hiloAuxiliar);
    }
    printf("Fin del programa\n");
    return 0;
}
