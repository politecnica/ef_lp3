#include "head.h"
int validarInput(int cantidadInput, char **argumentos){
    if(cantidadInput > 1){
        int indice = 1;
        int contadorNumeros = 0;

        int bandera = 1;

        while (indice < cantidadInput && bandera){
            int j = 0;
            while (argumentos[ indice ][ j ] != '\0' && bandera){
                if (!isdigit(argumentos[ indice ][ j ])){
                    bandera = 0;
                }
                else{
                    j++;
                }
            }
            if(bandera){
                contadorNumeros++;
                indice++;
            }
        }
        if(contadorNumeros == cantidadInput - 1){
            return 1;
        }
        else{
            printf("ERROR: El argumento numero:%d no es un digito numerico\n", indice);
            return 0;
        }
    }
    else{
        printf("Argumentos ingresados igual a 0\n");
        return 0;
    }
}
int* extraerNumeros(int longitudArgumentos, char **arrayArgumentos){
    int *arrayNumeros; //array de los numeros casteados
    int longitudArray; //cantidad de elementos
    int indiceArgumentos = 1; //indice para recorrer el array pasado como argumento, empieza en 1 porque el primer elemento es el nombre del programa

    longitudArray = longitudArgumentos - 1; //se le resta 1 porque uno de los elementos es el nombre del programa
    arrayNumeros = (int *) malloc( sizeof(int) * longitudArray ); //se asigna una direccion para el array

    for (int i = 0; i < longitudArray; i++){
        arrayNumeros[ i ] = atoi( arrayArgumentos[ indiceArgumentos ] ); //se va casteando elemento por elemento
        indiceArgumentos++; //se mueve el indice para el array de argumentos
    }
    return arrayNumeros; //retorna el puntero al array
}

long double factorial(int numero){
    //calcula el factorial iterativamente
    long double factorial = 1;
    if(numero == 0){
        return 1;
    }
    else{
        for(long double i = 1; i <= numero; i++){
        factorial *= i;
        }
        return factorial;
    }
}

void *funcionHilo(void *argumento){
    ptrHiloData hiloAux;
    long double numFactorial = 0;

    hiloAux = ((ptrHiloData) argumento);
    numFactorial = factorial(hiloAux->value);

    pthread_mutex_lock(&lock);
    contador++;
    pthread_cond_wait(&cv[ hiloAux->numHilo ], &lock);
    cond_recibida = 1;
    printf("Factorial de %d:%Lg\n", hiloAux->value, numFactorial);//se imprime el retorno del factorial
    pthread_mutex_unlock(&lock);
    return NULL;
}
