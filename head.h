#ifndef HEAD
#define HEAD
#include <stdio.h>
#include <stdlib.h>
#include <string.h> //atoi
#include <ctype.h> //isdigit
#include <pthread.h> //
#include <signal.h>
#include <unistd.h>

struct hiloData{
    int numHilo;
    int value;
};
typedef struct hiloData hiloData;
typedef hiloData * ptrHiloData;

int validarInput(int cantidadInput, char **argumentos);

int* extraerNumeros(int longitudArray, char **argumentos);
//extrae los numeros del array de cadenas casteandolos a numerico
long double factorial(int numero);
//calcula el factorial de un numero
void *funcionHilo(void *argumento);
//funcion que se va a pasar como argumento para la creacion de un hilo

pthread_mutex_t lock;
pthread_cond_t * cv;
int contador;
int cond_recibida;
//mutex para controlar el orden

#endif // HEAD
